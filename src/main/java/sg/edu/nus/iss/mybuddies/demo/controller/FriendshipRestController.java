package sg.edu.nus.iss.mybuddies.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sg.edu.nus.iss.mybuddies.demo.service.FriendshipService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

@RestController
@RequestMapping(path="/api/friendship", produces=MediaType.APPLICATION_JSON_VALUE)
public class FriendshipRestController {

    @Autowired
    private FriendshipService friendshipSvc;
    
}

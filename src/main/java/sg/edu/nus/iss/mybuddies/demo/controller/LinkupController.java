package sg.edu.nus.iss.mybuddies.demo.controller;

import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import sg.edu.nus.iss.mybuddies.demo.models.Linkup;
import sg.edu.nus.iss.mybuddies.demo.service.FriendshipService;

@Controller
@RequestMapping(path="/linkup")
public class LinkupController {

	@Autowired
    private FriendshipService friendshipSvc;


    @GetMapping()
	public String initLinkupForm(Map<String, Object> model) {
		model.put("linkup", new Linkup());
		return "linkup";
	}

	@PostMapping(consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String postLinkUp(
        @RequestParam String myemail, @RequestParam String friendEmail, Model model
    ){
		Linkup lk = new Linkup();
		lk.setMyemail(myemail);
		lk.setFriendEmail(friendEmail);
		friendshipSvc.createLinkUp(lk);
		return "result";
	}
    
}

package sg.edu.nus.iss.mybuddies.demo.repository;

import static sg.edu.nus.iss.mybuddies.demo.repository.SQL.*;

import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import sg.edu.nus.iss.mybuddies.demo.models.Friendship;
import sg.edu.nus.iss.mybuddies.demo.models.Linkup;

@Repository
public class FriendshipRepo {

    @Autowired
    private JdbcTemplate template;

    public Integer linkUpFriend(Linkup linkup){
        BigInteger myEmailContactId = BigInteger.valueOf(0);
        BigInteger friendEmailContactId = BigInteger.valueOf(0);
        BigInteger friendshipId = BigInteger.valueOf(0);

        Integer myEmailCount = template.query(SQL_SELECT_COUNT_CONTACT, 
            (ResultSet rs) -> {
                if(!rs.next())
                    return 0;
                final int count = rs.getInt("count");
                return Integer.valueOf(count);
            },
            linkup.getMyemail()
        );

        Integer friendEmailCount = template.query(SQL_SELECT_COUNT_CONTACT, 
            (ResultSet rs) -> {
                if(!rs.next())
                    return 0;
                final int count = rs.getInt("count");
                return Integer.valueOf(count);
            },
            linkup.getFriendEmail()
        );

        KeyHolder keyholder = new GeneratedKeyHolder();
        if(myEmailCount == 0){
            template.update(conn -> {
                PreparedStatement ps = conn.prepareStatement(SQL_INSERT_CONTACT, 
                    Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, linkup.getMyemail());
                return ps;
            }, keyholder);
            myEmailContactId = (BigInteger) keyholder.getKey();
        }else{
            Integer emailCtcId = template.query(SQL_SELECT_CONTACT_CONTACTID, 
                (ResultSet rs) -> {
                    if(!rs.next())
                        return 0;
                    final int idval = rs.getInt("contact_id");
                    return Integer.valueOf(idval);
                },
                linkup.getMyemail()
            );
            myEmailContactId = BigInteger.valueOf(emailCtcId);
        }
        
        keyholder = new GeneratedKeyHolder();
        if(friendEmailCount == 0){
            template.update(conn -> {
                PreparedStatement ps = conn.prepareStatement(SQL_INSERT_CONTACT, 
                    Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, linkup.getFriendEmail());
                return ps;
            }, keyholder);
            friendEmailContactId = (BigInteger) keyholder.getKey();
        }else{
            Integer emailCtcId = template.query(SQL_SELECT_CONTACT_CONTACTID, 
                (ResultSet rs) -> {
                    if(!rs.next())
                        return 0;
                    final int idval = rs.getInt("contact_id");
                    return Integer.valueOf(idval);
                },
                linkup.getFriendEmail()
            );
            friendEmailContactId = BigInteger.valueOf(emailCtcId);
        }
        System.out.println("" +myEmailCount.intValue());
        System.out.println("" + friendEmailCount.intValue());
        System.out.println("" +myEmailContactId.intValue());
        System.out.println("" + friendEmailContactId.intValue());
        
        if(myEmailCount.intValue() == 0 && friendEmailCount.intValue() == 0 ||
           myEmailCount.intValue() > 0 && friendEmailCount.intValue() == 0 ||
           myEmailCount.intValue() > 0 && friendEmailCount.intValue() > 0)
            if(myEmailContactId.intValue() > 0 && friendEmailContactId.intValue() > 0 ){
                keyholder = new GeneratedKeyHolder();
                final BigInteger x = myEmailContactId;
                final BigInteger y = friendEmailContactId;
                
                template.update(conn -> {
                    PreparedStatement ps = conn.prepareStatement(SQL_INSERT_FRIENDSHIP, 
                        Statement.RETURN_GENERATED_KEYS);
                    ps.setInt(1, x.intValue());
                    ps.setInt(2, y.intValue());
                    return ps;
                }, keyholder);
                friendshipId = (BigInteger) keyholder.getKey();
            }
        return friendshipId.intValue();
    }


    public Optional<Friendship[]> getAllMyFriendsByEmail(String myEmail){
        return template.query(
            SQL_SELECT_GET_ALL_MY_FRIENDS,
            (ResultSet rs) -> {
                List<Friendship> myfriends = new ArrayList<Friendship>();
                while(rs.next()){
                    String emailVal = rs.getString("email");
                    Integer friendshipId = rs.getInt("friendship_id");
                    Friendship f = new Friendship();
                    f.setEmail(emailVal);
                    f.setFriendshipId(friendshipId);
                    myfriends.add(f);
                }
                return Optional.of(myfriends.toArray(new Friendship[myfriends.size()]));
            },
            myEmail
        );
    }
}

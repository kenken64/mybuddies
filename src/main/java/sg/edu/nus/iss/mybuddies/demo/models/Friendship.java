package sg.edu.nus.iss.mybuddies.demo.models;

public class Friendship {
    private String email;
    private int friendshipId;
    
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public int getFriendshipId() {
        return friendshipId;
    }
    public void setFriendshipId(int friendshipId) {
        this.friendshipId = friendshipId;
    }
    
    
    
}

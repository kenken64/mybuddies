package sg.edu.nus.iss.mybuddies.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sg.edu.nus.iss.mybuddies.demo.models.Friendship;
import sg.edu.nus.iss.mybuddies.demo.models.Linkup;
import sg.edu.nus.iss.mybuddies.demo.service.FriendshipService;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class FriendsController {
    
	@Autowired
    private FriendshipService friendshipSvc;
	
    @GetMapping("/showListfriends")
	public String initListMyFriendsForm(Map<String, Object> model) {
		Friendship[] ff = new Friendship[0];
		model.put("linkup", new Linkup());
		model.put("myfriends", ff);
		return "listfriends";
	}

	@PostMapping("/listfriends")
	public String listallmyFriends(@RequestParam String myemail, Map<String, Object> model) {
		Friendship[] ff = friendshipSvc.getAllMyFriends(myemail);
		model.put("linkup", new Linkup());
		model.put("myfriends", ff);
		return "listfriends";
	}
}

package sg.edu.nus.iss.mybuddies.demo.models;

public class Linkup {
    private String myemail;
    private String friendEmail;
    
    public String getMyemail() {
        return myemail;
    }
    public void setMyemail(String myemail) {
        this.myemail = myemail;
    }
    public String getFriendEmail() {
        return friendEmail;
    }
    public void setFriendEmail(String friendEmail) {
        this.friendEmail = friendEmail;
    }
    
}

package sg.edu.nus.iss.mybuddies.demo.repository;

public interface SQL {

    public static final String SQL_INSERT_CONTACT =
    "insert into contact(email) values (?)";

    public static final String SQL_SELECT_COUNT_CONTACT =
    "select count(*) as count from contact where email = ?";

    public static final String SQL_SELECT_CONTACT_CONTACTID =
    "select contact_id from contact where email = ?";

    public static final String SQL_INSERT_FRIENDSHIP =
    "insert into friendship(contact_id, friend_contact_id) values (?,?)";

    public static final String SQL_SELECT_GET_ALL_MY_FRIENDS = 
    "select c.email, f.friendship_id from contact c, (select friend_contact_id, friendship_id from contact c , friendship f where c.contact_id = f.contact_id and c.email=?) f where c.contact_id = f.friend_contact_id";
     
    
}

package sg.edu.nus.iss.mybuddies.demo.service;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sg.edu.nus.iss.mybuddies.demo.models.Friendship;
import sg.edu.nus.iss.mybuddies.demo.models.Linkup;
import sg.edu.nus.iss.mybuddies.demo.repository.FriendshipRepo;

@Service
public class FriendshipService {

    @Autowired
    private FriendshipRepo friendshipRepo;

    public Integer createLinkUp(final Linkup lk){
        Integer friendshipId = friendshipRepo.linkUpFriend(lk);
        return friendshipId;
    }

    public Friendship[] getAllMyFriends(final String email){
        Optional<Friendship[]> op = friendshipRepo.getAllMyFriendsByEmail(email);
        return op.get();
    }
}

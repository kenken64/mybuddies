drop database if exists mybuddies;

create database mybuddies;

use mybuddies;

create table contact (
    contact_id int auto_increment not null,
    email varchar(64) not null,
    primary key(contact_id)
);

create table friendship (
	friendship_id int auto_increment not null,
    contact_id int not null,
    friend_contact_id int not null,
    is_block boolean,
    is_subscribe boolean,
    to_notify boolean,
    updates mediumtext,
    primary key(friendship_id),
    CONSTRAINT UC_friendship UNIQUE (contact_id,friend_contact_id)
);
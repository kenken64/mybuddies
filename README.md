## Friends Management ( MyBuddies )

```
mvn spring-boot:run
```

## Get all his friend's email by his email

```
select c.email, f.friendship_id from contact c, 
(select friend_contact_id, friendship_id from contact c , friendship f where c.contact_id = f.contact_id 
and c.email=?) f where c.contact_id = f.friend_contact_id

```

## Get friend which I can receive updates 

```
select email from contact c, 
(select f.friend_contact_id, f.is_subscribe from contact c , friendship f where c.contact_id = f.contact_id 
and email='bunnyppl@gmail.com') f where c.contact_id = f.friend_contact_id and f.is_subscribe is true;
```

## Find common friend from both given email address (Intersect)

```
select email from contact c, 
(select friend_contact_id from contact c , friendship f where c.contact_id = f.contact_id 
and email='bunnyppl@gmail.com') f where c.contact_id = f.friend_contact_id
and email in (
select email from contact c, 
(select friend_contact_id from contact c , friendship f where c.contact_id = f.contact_id 
and email='bunnyppl@hotmail.com') f where c.contact_id = f.friend_contact_id);
```